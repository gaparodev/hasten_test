//
//  Operations.swift
//  hastensport
//
//  Created by Gabriel Parente Rodriguez on 10/4/18.
//  Copyright © 2018 gparenter. All rights reserved.
//

import UIKit
import Foundation


class Operations: NSObject {


    class func getJSON() -> [[String : AnyObject]]? {
        
        let URLToGetFrom = "https://api.myjson.com/bins/66851"

        var arrJSON : [[String : AnyObject]]?
        
        do {
            
            let contents = try String(contentsOf: URL(string: URLToGetFrom)!)
            
            let data = contents.data(using: String.Encoding.utf8)
            
            arrJSON = try? JSONSerialization.jsonObject(with: data!, options: []) as! [[String : AnyObject]]
            
            return arrJSON
            
        } catch {
            
            print("Error parsing json")
            return nil
        }
    }
    
    
    
    class func organizeData(arrJSON: [[String : AnyObject]]) -> [[String : AnyObject]] {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate

        var arrObjects = [[String : AnyObject]]()

        var i = 0
        
        while i < arrJSON.count {
            
            if let players = arrJSON[i]["players"]! as? [[String:AnyObject]] {
                
                for player in players {
                    
                    arrObjects.append(player)
                }
            }
            
            appDelegate.arrTypes.append((arrJSON[i]["title"] as? String)!)
            
            i += 1
        }
                
        return arrObjects
    }
    
    
    
    class func filterPlayers(arrJSON: [[String : AnyObject]], filterToApply: String)  -> [[String : AnyObject]] {
        
        var arrObjects = [[String : AnyObject]]()

        var i = 0
        
        while i < arrJSON.count {
            
            if let sportTitles = arrJSON[i]["title"]! as? String {
                
                if sportTitles == filterToApply {
                    
                    if let players = arrJSON[i]["players"]! as? [[String:AnyObject]] {
                        
                        for player in players {
                            
                            arrObjects.append(player)
                        }
                    }
                }
            }
            
            i += 1
        }
        
        return arrObjects
    }
}
