//
//  ViewController.swift
//  hastensport
//
//  Created by Gabriel Parente Rodriguez on 10/4/18.
//  Copyright © 2018 gparenter. All rights reserved.
//

import UIKit
import Kingfisher


class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIPickerViewDelegate, UIPickerViewDataSource {
    
    
    @IBOutlet var tableView: UITableView!
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    var arrJSON : [[String : AnyObject]]?
    var arrObjects = [[String : AnyObject]]()
    
    var filterBy: String?
    
    
    
    
    //--------------------------------------
    // MARK: View Launch
    //--------------------------------------

    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        //Configure Navigation items
        self.navigationItem.title = "Hasten Sports"
        self.navigationItem.prompt = "Deportistas"
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        //Get objects
        self.arrJSON = Operations.getJSON()
        self.arrObjects = Operations.organizeData(arrJSON: self.arrJSON!)
        
        //Load Table
        self.tableView.reloadData()
        self.tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: UITableViewScrollPosition.top, animated: true)
    }
    
    
    
    
    
    //--------------------------------------
    // MARK: TableView
    //--------------------------------------
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.arrObjects.count
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CellSportsTVCell
        
        //Create cell
        cell = cell.createCell(cell: cell, playerToShow: self.arrObjects[indexPath.row])
        
        return cell
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    
    
    
    
    //--------------------------------------
    // MARK: PickerView To Filter
    //--------------------------------------
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        return 1
    }
    
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return self.appDelegate.arrTypes.count
    }
    
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return "\(self.appDelegate.arrTypes[row])"
    }
    
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        self.filterBy = self.appDelegate.arrTypes[row] as String
    }
    
    
    
    
    
    //--------------------------------------
    // MARK: Actions To Filter
    //--------------------------------------
    
    @IBAction func pickFilter(_ sender: AnyObject) {
        
        self.filterBy = self.appDelegate.arrTypes[0]
        
        let title = ""
        let message = "\n\n\n\n\n\n\n\n\n";
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert);
        alert.isModalInPopover = true;
        
        let pickerFrame: CGRect = CGRect(x: 17, y: 52, width: 233, height: 100)
        let picker: UIPickerView = UIPickerView(frame: pickerFrame)
        
        picker.delegate = self
        picker.dataSource = self
        
        //Add the picker to the alert controller
        alert.view.addSubview(picker)
        
        //Create the toolbar view - the view witch will hold our 2 buttons
        let toolFrame = CGRect(x: 17, y: 5, width: 270, height: 45)
        let toolView: UIView = UIView(frame: toolFrame)
        
        //add buttons to the view
        let buttonCancelFrame: CGRect = CGRect(x: 0, y: 7, width: 100, height: 30)
        let buttonOkFrame: CGRect = CGRect(x: 150, y: 7, width: 100, height: 30)
        
        //Create the cancel button & set its title
        let buttonCancel: UIButton = UIButton(frame: buttonCancelFrame)
        buttonCancel.setTitle(NSLocalizedString("CANCEL", comment: "Cancel"), for: UIControlState())
        buttonCancel.setTitleColor(UIColor.red, for: UIControlState())
        toolView.addSubview(buttonCancel) //add it to the toolView
        
        //Create the Select button & set the title
        let buttonOk: UIButton = UIButton(frame: buttonOkFrame)
        buttonOk.setTitle("OK", for: UIControlState())
        buttonOk.setTitleColor(UIColor.blue, for: UIControlState())
        toolView.addSubview(buttonOk) //add to the subview
        
        //Add the target - target, function to call, the event witch will trigger the function call
        buttonOk.addTarget(self, action: #selector(ViewController.confirmSelectionPicker), for: UIControlEvents.touchDown)
        buttonCancel.addTarget(self, action: #selector(ViewController.cancelSelectionPicker), for: UIControlEvents.touchDown)
        
        //add the toolbar to the alert controller
        alert.view.addSubview(toolView)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    
    
    @objc func confirmSelectionPicker() {
        
        if self.filterBy == "Todos" {
            
            //Get all objects
            self.viewWillAppear(true)
        }
        else {
            
            //Get filtered objects
            self.arrObjects = Operations.filterPlayers(arrJSON: self.arrJSON!, filterToApply: self.filterBy!)
        }
        
        self.dismiss(animated: true, completion: nil)
        
        //Load Table
        self.tableView.reloadData()
        self.tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: UITableViewScrollPosition.top, animated: true)
    }
    
    
    
    @objc func cancelSelectionPicker() {
        
        self.dismiss(animated: true, completion: nil)
    }
}

