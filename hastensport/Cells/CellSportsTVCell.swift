//
//  CellSportsTVCell.swift
//  hastensport
//
//  Created by Gabriel Parente Rodriguez on 10/4/18.
//  Copyright © 2018 gparenter. All rights reserved.
//

import UIKit

class CellSportsTVCell: UITableViewCell {
    
    
    @IBOutlet var cellImageUser: UIImageView!
    @IBOutlet var cellUserFirstName: UILabel!
    @IBOutlet var cellUserSurName: UILabel!

    
    override func awakeFromNib() {
        
        super.awakeFromNib()
    }

    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        
        super.setSelected(selected, animated: animated)
    }
    
    
    
    func createCell(cell: CellSportsTVCell, playerToShow: [String : AnyObject]) -> CellSportsTVCell {
        
        cell.cellUserFirstName.text = playerToShow["name"] as? String
        cell.cellUserSurName.text = playerToShow["surname"] as? String
        
        cell.cellImageUser.image = nil
        let imageToGet = playerToShow["image"] as? String
        cell.cellImageUser.kf.setImage(with: NSURL(string:imageToGet!)! as URL, placeholder: UIImage(named: ""))
        
        return cell
    }

}
